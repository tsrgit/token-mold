'use strict';

const fs = require('fs');
const path = require("path");

function getBaseDir() {
	return path.dirname(__dirname);
}

function readModuleJson() {
	return JSON.parse(fs.readFileSync(path.join(getBaseDir(), 'module.json')));
}

function getModuleName() {
	const tmp = path.basename(getBaseDir()).split("-").map(e => e.charAt(0).toUpperCase() + e.slice(1)).join(" ");
	return tmp[0].toLowerCase() + tmp.slice(1);
}

function setChangelogVersion(updateString) {
	const filePath = path.join(getBaseDir(), 'doc', 'changelog.txt');
	let changelog = fs.readFileSync(filePath);
	changelog = `# ${updateString}  
  
## Changelog:  
${changelog}`;

	fs.writeFileSync(filePath, changelog)
}

module.exports = {
	getBaseDir,
	readModuleJson,
	getModuleName,
	setChangelogVersion
}