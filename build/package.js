'use strict';

const fs = require('fs');
const path = require('path');
const archiver = require('archiver');
const util = require('./util');

(function zip() {
	const dir = util.getBaseDir();
	
	const moduleJson = util.readModuleJson(),
				name = moduleJson.name,
				distPath = path.join(dir, 'dist');
				
	if (!fs.existsSync(distPath))
		fs.mkdirSync(distPath);

	const zipPath = path.join(distPath, name+'.zip'),
				zipFile = fs.createWriteStream(zipPath),
				archive = archiver('zip');
	archive.pipe(zipFile);

	let arr = moduleJson.scripts || [];
	arr = arr.concat(moduleJson.styles || []);
	arr = arr.concat(moduleJson.packs || []);
	arr = arr.concat((moduleJson.languages || []).map(e => e.path))
	for (let obj of arr)
		archive.file(obj, {name: name+'/'+obj});

	archive.file('LICENSE', {name: name+'/LICENSE'});
	archive.file('module.json', {name: name+'/module.json'});

	// if (fs.existsSync(dir+'/lib'))
	// 	archive.directory('lib/', name+'/lib');
	if (fs.existsSync(dir+'/img'))
		archive.directory('img/', name+'/img');
	if (fs.existsSync(dir+'/res'))
		archive.directory('res/', name+'/res');
	if (fs.existsSync(dir+'/templates'))
		archive.directory('templates/', name+'/templates');
	if (fs.existsSync(dir+'/data'))
		archive.directory('data/', name+'/data');

	archive.finalize();
})()

// module.exports.package = zip;